# Ubuntu

This folder contains docker images for Ubuntu build machines.

## Dependencies

#### Build dependencies
 - cmake
 - g++
 - gcovr

#### vcpkg dependencies
 - curl
 - git
 - tar
 - unzip
 - zip

#### Glfw dependencies
 - libglu1-mesa-dev
 - libxcursor-dev
 - libxinerama-dev
 - xorg-dev

